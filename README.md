# SAR成像仿真系统

#### 已完成的功能

1. 点目标回波仿真

2. 后向传播（Back Projection, BP)成像算法

3. [RD成像算法](RangeDoppler.html)

### 待添加功能

- 多基地
- 多种目标类型
- 稀疏成像算法

### Sublime Text Build Systems 

1. 在 sublime-project 里设置 "build_systems", 必须有 "name" 属性