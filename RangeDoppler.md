---
title: RD成像算法
---

### 回波模型

发射信号
$$
\DeclareMathOperator{\rect}{rect}
s_0(t) = \rect(\frac{t}{T_p})\exp(j\pi k t^2)
$$

$$
s_a(\eta; x_t) = \rect(\frac{x_0 + v\eta - x_t}{L_s})
$$

$$
x(t, \eta; x_t) = s_0(t - \frac{2R(x_0 + v\eta; x_t)}{c}) s_a(\eta)
$$

### 远场假设

$$
\begin{aligned}
R(x; x_t) &= \sqrt{R_0^2 + (x - x_t)^2} 
= R_0 \left[1 + \frac{(x - x_t)^2}{R_0^2}\right]^{\frac{1}{2}}
\\
&\approx R_0 + \frac{1}{2}\frac{(x - x_t)^2}{R_0}
\end{aligned}
$$

### 距离徙动

系统响应函数在纵向(距离向)的移动称为**距离徙动**。

在正侧视情况下，将合成阵列中心与有效阵列两侧到中间目标的距离差称为**响应函数的距离弯曲**, 简称**距离弯曲**。

在斜视情况下，在合成阵列某处 $x$ 到目标的距离$R(x; x_t)$与阵列中心($x=0$)到该目标的距离$R(0)$之差
$$
\Delta{R} = R(x) - R(0) 
\approx \left.\frac{dR}{dx}\right|_{x=0} x
+ \frac{1}{2}\left.\frac{d^2R}{dx^2}\right|_{x=0} x^2
$$

由$x$的一次项和二次项所组成，通常把一次项（即线性项）称为**距离走动**，而将二次项称为**距离弯曲**。

### 算法分情况讨论

根据实际雷达参数和分辨率要求，对距离徙动对包络时延影响的考虑可分四种情况，

① "NoRangeMigration":
距离徙动不考虑，距离和方位可分维处理；
$$
x(t, \eta) \approx  s_r(t) s_a(\eta)
% \rect(\frac{v\eta - x_0}{L_s})\rect(\frac{t - \frac{2R_0}{c}}{T_p})\exp(j\pi k(t - \frac{2R_0}{c})^2)\exp(-2j\pi f_c \frac{2R(\eta)}{c}) \triangleq
$$
其中
$$
\begin{aligned}
 s_r(t)
 & = s_0(t - \frac{2R_0}{c})
 = \rect\left(\frac{t - \frac{2R_0}{c}}{T_p}\right)\exp(j\pi k(t - \frac{2R_0}{c})^2)
 \\
 s_a(\eta)
 & = 
 \rect(\frac{x_0 + v\eta - x_t}{L_s})\exp(-2j\pi f_c \frac{2R(\eta; x_t)}{c})
\end{aligned}
$$

② "RangeWalkOnly":
考虑距离走动，距离弯曲不考虑，在观测的场景里距离走动率对不同的纵向距离是相同的;
在时域解耦合
$$
R(\eta) \approx R_0 + \frac{1}{2R_0}(x_0 + v\eta - x_t)^2
\approx R_0 - \frac{x_0 - x_t}{R_0}v\eta
$$

$$
x(t,\eta)=s_0(t - \frac{2R(\eta)}{c}) s_a(\eta)
$$

$$
\tilde{x}(t, \eta)=
x(t + \frac{2(R(\eta) - R_0)}{c}) 
\approx 
s_r(t - \frac{2R_0}{c})s_a(\eta)
% \rect(\frac{v\eta - x_0}{L_s})\rect(\frac{t - \frac{2R_0}{c}}{T_p})\exp(j\pi k(t - \frac{2R_0}{c})^2)\exp(-2j\pi f_c \frac{2R(\eta)}{c})
$$
或
$$
\tilde{X}(f, \eta) = X(f, \eta)\exp(j2\pi f\frac{2(R(\eta) - R_0)}{c})
= S_0(f)s_a(\eta)\exp(j2\pi f\frac{2(R(\eta) - R_0)}{c})
$$
③ "FixedRangeCurvature": 
距离走动和距离弯曲都考虑，但场景内各处的距离弯曲近似相同；
在多普勒域解耦合

$$
R(\eta) \approx R_0 + \frac{1}{2R_0}(x_0 + v\eta - x_t)^2
\approx R_0 + \frac{2v^2}{R_0}(\eta - \frac{x_t - x_0}{v})^2
$$

$$
x(t, \eta) = s_0(t - \frac{2R(\eta)}{c})s_a(\eta)
$$

$$
X(f_t, \eta) = S_0(f_t)\exp(-j2\pi f_t\frac{2R(\eta)}{c})s_a(\eta)
\approx 
S_0(f_t)\exp(-j2\pi f_t\frac{2R_0}{c})
s_a(\eta)\exp(-j2\pi f_c\frac{2}{c}\frac{2v^2}{R_0}(\eta - \frac{x_t - x_0}{v})^2)
$$
④ "DependentRangeCurvature":
距离走动和距离弯曲都考虑，且场景内的距离弯曲差不能忽略。
Chirp Scaling, RMA