function signal = newSignal(options, frequency, fc)
	method = ['new', options.Type];
	signal = feval(method, options);
end
function signal = newChirp(options)
	signal = options;
	signal.getSamples = @getSamples;
	function s = getSamples(frequency, fc)
		num = numel(frequency);
		df = (frequency(end) - frequency(1))/(num - 1); 
		fs = num * df;
		s.Time = (0:1/fs:(1/df - 1/fs))';
		t = s.Time - signal.PulseWidth/2;
		k = signal.BandWidth/signal.PulseWidth;
		Phi = k/2*t.^2;
		s.Amplitude =  (s.Time <= signal.PulseWidth) .* exp(2j*pi*Phi);
	    s.CarrierFrequency = fc;
		[s.Spectrum, s.Frequency] = doFFT(s.Amplitude, s.Time, 1, 'padding', 0);
	end
end
