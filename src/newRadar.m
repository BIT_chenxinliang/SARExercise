function radar = newRadar(varargin)
% 创建雷达对象
	c = 3e8;
	radar = struct(varargin{:});
	radar.frequency = @getFrequency;
	radar.locations = @getLocations;
	radar.getAmplitude = @getAmplitude;
	radar.emitSignal = @emitSignal;

	%% emitSignal:  计算发射信号
	function signal = emitSignal(varargin)
		options = struct(varargin{:});
		if isfield(options, 'SampleFrequency')
			fs = options.SampleFrequency;
		else
			fs = options.Signal.BandWidth;
		end
		T = options.Rwindow*2/c + options.Signal.PulseWidth*2;
		[frequency, fc] = getFrequency(fs, T);
		s = newSignal(options.Signal);
		signal = s.getSamples(frequency, fc);
	end
	%% getFrequency: 计算雷达的频率范围（采样点）
	function [frequency, fc] = getFrequency(fs, T)
		fc = c / radar.WaveLength;
		nFrequency = ceil(fs * T/2)*2;
		df = fs / nFrequency;
		frequency = (fc - fs/2 + (0:nFrequency - 1)*df)';
	end
	%% getAmplitude: 计算目标是否在波束内
	function [amplitude] = getAmplitude(location)
		product = @(x, y) sum(x.*y);
		norm = @(x) sqrt(product(x, x));
		normalizedProduct = @(x, y) product(x, y) ./ (norm(x) * norm(y));
		angle = acos(normalizedProduct(location, radar.Trajectory.Velocity));
		beamWidth = radar.BeamWidth;
		if isfield(radar, 'BeamWidthUnit') && strcmp(radar.BeamWidthUnit, 'degree')
			beamWidth = radar.BeamWidth * pi/180;
		end
		amplitude = abs(angle - pi/2) < radar.BeamWidth/2;
	end
	%% getLocations: 计算雷达的航迹位置
	function [locations, slowTime] = getLocations()
		start = radar.Trajectory.StartLocation;
		velocity = radar.Trajectory.Velocity;
		duration = radar.Trajectory.Duration;
		prf = radar.PRF;
	    nSlowTime = ceil(prf * duration/2)*2;
	    slowTime = (0:nSlowTime - 1)/prf - duration/2;
		locations = start + velocity*slowTime;    
	end
end
