function image = RangeDoppler(echo, varargin)
    c = 3e8;
	options = struct(varargin{:});
    profiles = PulseCompression(echo, options);
    %% ��ʱ��Ƶ��-������
    if isfield(options, 'ShowFrequencyPulse') && options.ShowFrequencyPulse
        figure('Name', 'FrequencyPulse', 'NumberTitle', 'off', 'WindowStyle', 'docked');
        subplot(221);
        set(gca, 'YDir', 'normal');
        imagesc(profiles.SlowTime, profiles.Frequency, abs(profiles.Spectrum));
        xlabel('Slow Time[s]');
        ylabel('Frequency[Hz]');
        freqIndex = ceil(numel(profiles.Frequency)/2);
        subplot(222);
        plot(profiles.SlowTime, abs(profiles.Spectrum(freqIndex,:)), profiles.SlowTime, real(profiles.Spectrum(freqIndex,:)));
        xlabel('Slow Time[s]');
        title(sprintf('Frequency[%d] = %f ', freqIndex, profiles.Frequency(freqIndex)));
        subplot(223);
        slowIndex = ceil(numel(profiles.SlowTime)/2);
        [~, rangeIndex] = max(abs(profiles.Amplitude(:, slowIndex)));
        plot(profiles.Frequency, real(profiles.Spectrum(:, slowIndex).*exp(2j*pi*profiles.Frequency*2*profiles.Range(rangeIndex)/c)));
        xlabel('Frequency[Hz]');
        title(sprintf('Compensated by range %f ', profiles.Range(rangeIndex))); 
        drawnow;
    end
    %% Ƶ��-��������
    if isfield(options, 'DopplerStep')
        df = options.DopplerStep;
        nPRT = numel(echo.SlowTime);
        prf = (nPRT - 1)/(echo.SlowTime(end) - echo.SlowTime(1));
        padding = prf/df - nPRT;
    else
        padding = [];
    end
    [image.FrequencyDoppler, image.Doppler] = doFFT(profiles.Spectrum, echo.SlowTime, 2, 'padding', padding);
    image.Frequency = profiles.Frequency;
    if isfield(options, 'ShowFrequencyDoppler') && options.ShowFrequencyDoppler
        figure('Name', 'FrequencyDoppler', 'NumberTitle', 'off', 'WindowStyle', 'docked');
        set(gca, 'YDir', 'normal');
        imagesc(image.Doppler, image.Frequency, abs(image.FrequencyDoppler));
        xlabel('Doppler[Hz]');
        ylabel('Frequency[Hz]');
    end
    %% �����������
    [image.RangeDoppler, image.Doppler] = doFFT(profiles.Amplitude, echo.SlowTime, 2, 'padding', padding);
    image.SlantRange = echo.FastTime*c/2;
    if isfield(options, 'ShowRangeDoppler') && options.ShowRangeDoppler
        figure('Name', 'RangeDoppler', 'NumberTitle', 'off', 'WindowStyle', 'docked');
        subplot(211);
        imagesc(image.Doppler, image.SlantRange, abs(image.RangeDoppler));
        set(gca, 'YDir', 'normal');
        xlabel('Doppler[Hz]');
        ylabel('Slant Range[m]');
        subplot(212);
        [peaks, rangeIndex] = max(abs(image.RangeDoppler));
        plot(image.Doppler, real(image.RangeDoppler(rangeIndex(1),:)));
        drawnow;
    end
    fdr = 2*options.Speed^2/options.WaveLength/options.Rref;
    phase = -image.Doppler.^2/fdr/2; 
    [image.Image, slowTime] = doIFFT(image.RangeDoppler.*exp(2j*pi*phase), image.Doppler, 2);
    image.CrossRange = slowTime*options.Speed;
    if ~isfield(options, 'ShowImage') || options.ShowImage
        figure('name', 'Image', 'NumberTitle', 'off', 'WindowStyle', 'docked');
        imagesc(image.CrossRange, image.SlantRange, abs(image.Image));
        set(gca, 'YDir', 'normal');
        xlabel('Cross Range[m]');
        ylabel('Slant Range[m]');
        drawnow;
    end
    if isfield(options, 'OutputFile')
        save(options.OutputFile, 'echo', 'image');
    end
end
