%% doIFFT �渵��Ҷ�任
function [amplitude, time] = doIFFT(spectrum, frequency, dim, varargin)
	options = struct(varargin{:});
	NumOfSamples = numel(frequency);
	assert(size(spectrum, dim) == NumOfSamples);
	df = (frequency(end) - frequency(1))/(NumOfSamples - 1);
    f1 = (-NumOfSamples/2:NumOfSamples/2 - 1)'*df;
% 	assert(max(abs(frequency(:) - f1))/df < 1e-5);
	if ~isfield(options, 'padding')
		options.padding = 2^nextpow2(NumOfSamples) - NumOfSamples;
	end
	if isfield(options, 'offset') 
		spectrum = spectrum .* exp(2j*pi*frequency*options.offset);
	else
		options.offset = 0;
	end
	if options.padding > 0	% ����
		halfPadding = round(options.padding/2);
		assert(halfPadding*2 == options.padding);
		shape = size(spectrum);
		shape(dim) = halfPadding;
		if dim == 1
			spectrum = [zeros(shape); spectrum; zeros(shape)];
		else
			spectrum = [zeros(shape), spectrum, zeros(shape)];
        end
    else
        assert(options.padding >= 0, 'NegativePadding');
    end
    nfft = size(spectrum, dim);
	time = options.offset + (0:nfft-1)/nfft/df;
	amplitude = ifft(ifftshift(spectrum, dim), nfft, dim);
end