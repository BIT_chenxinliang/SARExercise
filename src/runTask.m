function results = runTask(task, dataDir)
% 运行仿真  
    if ~isempty(task)
        if ischar(task)
            task = loadTask(task, dataDir);
        end
        if isstruct(task)
            echo = getEcho(task.Radars, task.Targets, task.EchoOptions);
            results = getSARImage(echo, task.ImagingOptions);
        end
    end
end
function [task] = loadTask(rcFile,dataDir)
%   加载各种仿真参数
    if ~exist('rcFile', 'var')
        rcFile = 'settings.json';
    end
    if ~exist('dataDir', 'var')
        dataDir = '../data';
    end
    rcFilePath = fullfile(dataDir, rcFile);
    if ~exist(rcFilePath, 'file')
        filters = {'*.json', 'JSON文件(*.json)'};
        title = '选择配置文件';
        [rcFile, dataDir] = uigetfile(filters, title, rcFilePath);
        rcFilePath = fullfile(dataDir, rcFile);
    end
    if exist(rcFilePath, 'file')
        fileID = fopen(rcFilePath, 'r');
        json = fread(fileID, 'uint8=>char')';
        task = jsondecode(json);
        fclose(fileID);
    else
        task = [];
        warning('FileNotFound', 'cannot found the task file %s', rcFilePath);
    end
end