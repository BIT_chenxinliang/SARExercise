function echo = getEcho(radars, targets, options)
% 生成回波
	if nargin < 3
		options = struct();
	end
	c = 3e8;
	nRadar = numel(radars);
	if nRadar > 1
		echo = cell(nRadar, 1);
		for iRadar = 1:nRadar
			echo{iRadar} = getEcho(radars(iRadar), targets, options);
		end
	else
		radar = newRadar(radars);
		signal = radar.emitSignal(options);	
		if isfield(options, 'ShowSignal') && options.ShowSignal
			figure('Name', 'Signal', 'NumberTitle', 'off', 'WindowStyle', 'docked');
			subplot(121);
			plot(signal.Time, real(signal.Amplitude));
			xlabel('Time[s]');
			ylabel('Amplitude');
			subplot(122);
			plot(signal.Frequency, abs(signal.Spectrum));
			xlabel('Frequency[Hz]');
			ylabel('Spectrum');
        end

		echo = getTargetResponse(targets, radar, signal);
		if isfield(options, 'ShowScene') && options.ShowScene
			figure('Name', 'Scene', 'NumberTitle', 'off', 'WindowStyle', 'docked');
			line(echo.Locations(1,:), echo.Locations(2,:));
			hold on;
			scatter(echo.TargetLocations(1,:), echo.TargetLocations(2,:), '*');
			xlim(extendInterval(xlim, 0.1));
			ylim(extendInterval(ylim, 0.1));
			xlabel('X[m]');
			ylabel('Y[m]');
			title('观测场景');
			legend('雷达航迹', '目标', 'Location', 'Best');
		end
		[echo.Amplitude, echo.FastTime] = doIFFT(echo.Spectrum, echo.Frequency, 1, ...
			'offset', options.Rmin*2/c);
		if isfield(options, 'ShowEcho2D') && options.ShowEcho2D
			figure('Name', 'Echo2D', 'NumberTitle', 'off', 'WindowStyle', 'docked');
			subplot(121);
			imagesc(echo.SlowTime, echo.FastTime, (real(echo.Amplitude)));
			set(gca, 'YDir', 'normal');
			xlabel('Slow Time[s]');
			ylabel('Fast Time[s]');
			subplot(122);
			imagesc(echo.SlowTime, echo.Frequency, (abs(echo.Spectrum)));
			set(gca, 'YDir', 'normal');
			xlabel('SlowTime[s]');
			ylabel('Frequency[Hz]');
			title('Spectrum');
		end
	end
	if ~isfield(options, 'Verbose') || options.Verbose
		disp('回波生成完毕！');
	end
	if isfield(options, 'OutputFile')
		save(options.OutputFile, 'echo', 'radars', 'targets', 'options');
	end
end

function newbounds = extendInterval(bounds, border)
	width = bounds(2) - bounds(1);
	newbounds = [bounds(1) - border*width/2, bounds(2) + border*width/2];
end

function response = getTargetResponse(targets, radar, signal)
	% 计算目标的频谱响应
	response.Frequency = signal.Frequency;
	response.CarrierFrequency = signal.CarrierFrequency;
	[response.Locations, response.SlowTime] = radar.locations();
	nPosition = size(response.Locations, 2);
	nFrequency = numel(response.Frequency);
	response.Spectrum = zeros(nFrequency, nPosition);
	nTargets = numel(targets);		
	response.TrueRange = nan(nTargets, nPosition);
	response.TargetLocations = zeros(3, nTargets);
	for iTarget = 1:nTargets 
		target = newTarget(targets(iTarget));
		[Spectrum, R, targetLocation] = target.getEchoSpectrum(radar, signal.Frequency);
		response.TargetLocations(:, iTarget) = targetLocation;
        response.TrueRange(iTarget, :) = R;
		response.Spectrum = response.Spectrum + Spectrum;
	end
	response.Spectrum = signal.Spectrum .* response.Spectrum;
end