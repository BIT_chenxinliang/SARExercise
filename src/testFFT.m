% testFFT ����FFT
function tests = testFFT
	tests = functiontests(localfunctions);
end

function [s, t] = setup(testCase)  % do not change function name
end

function teardown(testCase)  % do not change function name
% change back to original path, for example
end

function testRecovery(testCase)
	duration = 2;
	fs = 1e3;
	k = 400;
	Tp = 1;
	% t0 = 0;
	% fc = 0;
	[s, t] = chirp(duration, fs, k, Tp); 
	[S, f] = doFFT(s, t, 2);
	[s1, t1] = doIFFT(S, f, 2);
	[S1, f1] = doFFT(s1, t1, 2);
	% fprintf('len(t) = %d, len(t1) = %d\n', numel(t), numel(t1));
	fig = figure(1);
	set(fig, 'Name', 'testRecovery', 'WindowStyle', 'docked');
	subplot(121);
	plot(t, real(s), 'b.-', t1, real(s1), 'ro-');
	subplot(122);
	plot(f, real(S), 'b.-', f1, real(S1), 'ro-');
end

%% testForward
function testForward(testCase)
	duration = 2;
	fs = 1e3;
	k = 400;
	Tp = 1;
	t0 = 0;
	% fc = 0;
	[s, t] = chirp(duration, fs, k, Tp, t0); 
	[S, f] = doFFT(s, t, 2);
	S2 = sum(s .* exp(-2j*pi*f(:).*t), 2);
	S3 = fftshift(fft(s, numel(f), 2), 2);
	[S1, f1] = chirp(fs, numel(f)/fs, 1/k, k*Tp, 0);
	fig = figure('Name', 'TestForward', 'NumberTitle', 'off', 'WindowStyle', 'docked');
	clf(fig);
	subplot(121);
	plot(t, real(s), 'b', t, imag(s), 'r');
	subplot(122);
	plot(f, real(S - S2.'), f, real(S3 - S));
end

function testChirp(testCase)
	duration = 2;
	fs = 1e3;
    B = fs/2;
	k = 1e3;
	Tp = B/k;
	t0 = 0;
	% fc = 0;
	[s, t] = chirp(duration, fs, k, Tp, t0); 
	num = numel(s);
	zeropadding = 2^(nextpow2(num)+4) - num;
	[S, f] = doFFT(s, t, 2, zeropadding);
    phase = f.^2/k -1/4;
	S1 = fs/sqrt(k)*rectpuls(f/(k*Tp)).*exp(-1j*phase);
	fig = figure('Name', 'TestChirp', 'NumberTitle', 'off', 'WindowStyle', 'docked');
	clf(fig);
	fig.NumberTitle = 'off';
	fig.Name = 'testForward';
    plot(f, real(S.*exp(1j*pi*phase)), 'b.-', f, imag(S.*exp(1j*pi*phase)), 'r');  	
end

%% testSine: �������Һ���
function  testSine(testCase)
	t = 0:999;
	s = rectpuls((t-500)/200).*exp(1j*pi/20*t);
	[S, f] = doFFT(s, t, 2, 0);
	nfft = numel(f);
	S1 = sum(s .* exp(-1j*2*pi*f(:).*(0:nfft - 1)), 2).';
	assert(max(abs(S1 - S)) < 1e-10);
end
