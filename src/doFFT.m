%% doFFT ����Ҷ�任
function [spectrum, frequency] = doFFT(signal, time, dim, varargin)
	options = struct(varargin{:});
	NumOfSamples = size(signal, dim);
	assert(numel(time) == NumOfSamples);
	fs = (NumOfSamples - 1) / (time(end) - time(1));
	if ~isfield(options, 'padding') || isempty(options.padding)
		options.padding = 2^nextpow2(NumOfSamples) - NumOfSamples;
	end
	nfft = NumOfSamples + options.padding;
	frequency = (-nfft/2:nfft/2-1)/nfft*fs;
    if dim == 1
        frequency = frequency(:);
    end
	spectrum = fftshift(fft(signal, nfft, dim), dim);
	spectrum = spectrum .* exp(-2j*pi*frequency*time(1));
end