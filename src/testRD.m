%% 测试RD算法
if ~exist('echo', 'var')
	clear; 
	load echo;
end
close all; clc;
%% 时域回波
figure('Name', '时域回波', 'WindowStyle', 'docked');
subplot(211);
set(gca, 'YDir', 'normal');
imagesc(echo.SlowTime, echo.FastTime, real(echo.Amplitude));
xlabel('Slow Time[s]');
ylabel('Fast Time[s]');
% 验证回波
R0 = radars.Trajectory.StartLocation - targets.Location
subplot(212);
plot(echo.FastTime, real(echo.Amplitude(:,1)));
xlabel('Fast Time[s]');
ylabel('Amplitude');
drawnow;