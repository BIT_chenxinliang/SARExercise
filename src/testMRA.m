N = 100;
M = 80;
t0 = (0:N-1)/N;
x0 = randn(N,1) + 1j*randn(N,1);
% x0(:) = 0;
% x0(3) = 1;
% x0(15) = 4;
f = (0:M-1)';
A0 = exp(-2j*pi*f*t0);
y0 = A0 * x0;
t1 = (0:(N/2 - 1))/(N/2);
A1 = exp(-2j*pi*f*t1);
x1 = A1\y0; 
% x1 = A1' * y0;
% solve:  y0 = A1 * x; = inv(A1)*x;
plot(t0, real(x0), t0, imag(x0)); hold on;
plot(t1, real(x1), t1, imag(x1));
hold off;