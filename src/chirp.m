%% chirp ���Ե�Ƶ�ź�
function [s, t] = chirp(duration, fs, k, T, t0, fc)
	if nargin < 6
		fc = 0;
		if nargin < 5
			t0 = duration/2;
			if nargin < 4
				T = duration;
			end
		end
	end
	t = t0-duration/2 + (0:ceil(duration*fs - 1))/fs;
	s = rectpuls((t - t0)/T).*exp(1j*(pi*k*(t - t0).^2 + fc*(t - t0)));
end