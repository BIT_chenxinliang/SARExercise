%% getSARImage SAR成像
function [image] = getSARImage(echo, options)
	if ~isfield(options, 'ImagingMethod')
		options.ImagingMethod = 'BackProjection';
    end
    image = feval(options.ImagingMethod, echo, options);
end

%% BackProjection: 后向传播算法
function [image] = BackProjection(echo, radar, options)
	num = @(X) floor((X.Stop - X.Start)/X.Stride) + 1;
	grid= @(X) X.Start + (0:num(X) - 1)*X.Stride;
	image.X = grid(options.X);
	image.Y = grid(options.Y);
	nX = numel(image.X);
	nY = numel(image.Y);
	image.Data = zeros(nY, nX);
	for iX = 1:nX
		for iY = 1:nY
			imaginaryTarget = struct(...
				'Type', 'Point', ...
				'Location', [image.X(iX); image.Y(iY); 0] ...
				);
			S = getEcho(radar, imaginaryTarget, struct('Verbose', false));
			image.Data(iY, iX) = sum(sum(echo.Spectrum.*conj(S.Spectrum)));
		end
		fprintf('BackProjection: %d / %d \n', iX, nX);
	end
	if ~isfield(options, 'ShowImage') || options.ShowImage
		figure('Name', 'BackProjection', 'NumberTitle', 'off', 'WindowStyle', 'docked');
		imagesc(image.X, image.Y, abs(image.Data));
		xlabel('X[m]');
		ylabel('Y[m]');
		drawnow;
	end
end

