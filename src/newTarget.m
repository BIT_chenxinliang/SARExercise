%% 创建目标对象
function [target] = newTarget(options)
	target.getEchoSpectrum = @getEchoSpectrum;
	target.location = @() options.Location;
	function [Spectrum, R, targetLocation] = getEchoSpectrum(radar, frequency)
		radarLocations = radar.locations();
		targetLocation = options.Location;
		relativePosition = targetLocation - radarLocations;
		R = sqrt(sum(relativePosition.^2));
		amplitude = radar.getAmplitude(relativePosition);
		c = 3e8;
		Spectrum = amplitude.*exp(-2j*pi*frequency(:)*2*R/c);
	end
end
